import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  mensaje1 = 'Este es el primer mensaje';
  mensaje2 = 'Este es el segundo mensaje';
  mensaje3 = 'Este es el tercer mensaje';
  mensaje4 = 'Este es el cuarto mensaje';
  mensaje5 = 'Este es el quinto mensaje'
  
  constructor() { }

  ngOnInit(): void {
  }

}
